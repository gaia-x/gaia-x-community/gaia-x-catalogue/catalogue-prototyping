const fs = require('fs')

// Start up the Graph Database
var sparql = require('./graph');
let {graph, query_builder} = sparql.createGraph()

// Load the VC import functions
const vc = require('@digitalbazaar/vc');

// Load the JSON parsing functions
const jsonld = require('jsonld');

// Prevent having to download contexts
const CONTEXTS = {
    "https://w3id.org/credentials/v1": JSON.parse(fs.readFileSync('./contexts/w3id_credentials.json')),
    "https://w3id.org/security/v1": JSON.parse(fs.readFileSync('./contexts/w3id_security_v1.json')),
    "https://w3id.org/security/v2": JSON.parse(fs.readFileSync('./contexts/w3id_security_v2.json'))
};
jsonld.documentLoader = async (url, options) => {
    if(url in CONTEXTS) {
        return {contextUrl: null, document: CONTEXTS[url], documentUrl: url};
    }
    return nodeDocumentLoader(url);
};

// Load a Self-Descriptions into the Graph
function loadSD(filename, content) {
    vc_json = JSON.parse(content);
    console.log('Loading SD ' + filename + ' for the Entity ' + vc_json.id);
    (async () =>{
        const nquads = await jsonld.toRDF(vc_json, {format: 'application/n-quads'});
        console.log(nquads);
        graph.parse(nquads)
        console.log(filename + " inserted into triple-store")
    })();
}

// Load all SD from the directory
fs.readdirSync('./sd-cache').map(filename => {
    loadSD(filename, fs.readFileSync('./sd-cache/' + filename, 'utf8'))
});

// REST interface
const express = require('express');
const app     = express();
const port    = 5000;

app.get('/', (req, res) => {
    res.sendFile('index.html', {root: __dirname});
});

app.get('/query', async (req, res) => {
    console.log("Query: " + req.query.query);
    try {
        res.setHeader('Content-Type', 'application/json');
        const iterator = query_builder.build(req.query.query)
        result_array = []
        iterator.subscribe(
            bindings => {
                result_array.push(bindings.toObject()) },
            err => console.error(err),
            () => {
                console.log("Results:")
                console.log(result_array)
                res.status(200).send(result_array)
                console.log('Query evaluation complete!')
            }
        )
    } catch(e) {
        console.log(e);
        res.status(200).send("Invalid Query");
    }
});

app.listen(port, () => {console.log(`Now listening on port ${port}`);});
