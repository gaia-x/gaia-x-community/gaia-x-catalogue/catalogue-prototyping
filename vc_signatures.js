'use strict'

const fs = require('fs')
const jsonld = require('jsonld');

// mock data
const controller = 'https://example.edu/issuers/565049';

const mockPublicKey2020 = {
  '@context': 'https://w3id.org/security/suites/ed25519-2020/v1',
  type: 'Ed25519VerificationKey2020',
  controller,
  id: controller + '#z6MknCCLeeHBUaHu4aHSVLDCYQW9gjVJ7a63FpMvtuVMy53T',
  publicKeyMultibase: 'z6MknCCLeeHBUaHu4aHSVLDCYQW9gjVJ7a63FpMvtuVMy53T'
};

const controllerDoc2020 = {
  '@context': [
    'https://www.w3.org/ns/did/v1',
    'https://w3id.org/security/suites/ed25519-2020/v1'
  ],
  id: 'https://example.edu/issuers/565049',
  assertionMethod: [mockPublicKey2020]
};

const CONTEXTS = {
    "https://w3id.org/credentials/v1": JSON.parse(fs.readFileSync('./contexts/w3id_credentials.json')),
    "https://w3id.org/security/v1": JSON.parse(fs.readFileSync('./contexts/w3id_security_v1.json')),
    "https://w3id.org/security/v2": JSON.parse(fs.readFileSync('./contexts/w3id_security_v2.json')),
    "https://example.edu/issuers/565049#z6MknCCLeeHBUaHu4aHSVLDCYQW9gjVJ7a63FpMvtuVMy53T": mockPublicKey2020,
    "https://example.edu/issuers/565049": controllerDoc2020
};
let myDocumentLoader = async (url, options) => {
    if(url in CONTEXTS) {
        return {contextUrl: null, document: CONTEXTS[url], documentUrl: url};
    }
    return jsonld.documentLoader(url);
};

// Load the JSON-LD import functions
let jsigs = require('jsonld-signatures');
const {purposes: {AssertionProofPurpose}} = jsigs;
let {Ed25519VerificationKey2020} = require('@digitalbazaar/ed25519-verification-key-2020');
let {Ed25519Signature2020} = require('@digitalbazaar/ed25519-signature-2020');

// sign a JSON-LD document
async function signVP(vp, keySuite) {
    let signed = await jsigs.sign(vp, {
        suite: keySuite,
        purpose: new AssertionProofPurpose(),
        documentLoader: myDocumentLoader
    });
    return signed
}

// build a VP verification instance
// TODO: Make use of whitelist information
function buildVPSigChecker(whitelist) {
    let check_fun = async (svp) => {
        return await jsigs.verify(svp, {
            suite: new Ed25519Signature2020(),
            purpose: new AssertionProofPurpose(),
            documentLoader: myDocumentLoader
        });
    }
    return check_fun
}

// export functions
module.exports = {
    signVP: signVP,
    buildVPSigChecker: buildVPSigChecker
}

async function test() {
    // create the keypair to use when signing
    // has to match to the mock data (for now)
    let keyPair = await Ed25519VerificationKey2020.from({
        type: 'Ed25519VerificationKey2020',
        controller,
        id: controller + '#z6MknCCLeeHBUaHu4aHSVLDCYQW9gjVJ7a63FpMvtuVMy53T',
        publicKeyMultibase: 'z6MknCCLeeHBUaHu4aHSVLDCYQW9gjVJ7a63FpMvtuVMy53T',
        privateKeyMultibase: 'zrv2EET2WWZ8T1Jbg4fEH5cQxhbUS22XxdweypUbjWVzv1YD6VqYu' + 'W6LH7heQCNYQCuoKaDwvv2qCWz3uBzG2xesqmf'
    });
    let suite = new Ed25519Signature2020({key: keyPair});

    console.log("Sign the JSON-LD Document")
    let file = process.argv[2]
    let vp = JSON.parse(fs.readFileSync(file))
    let svp = await signVP(vp, suite);
    console.log(svp);

    console.log("Verify the JSON-LD Document")
    let whitelist = []
    let checker = buildVPSigChecker(whitelist)
    let check_res = await checker(svp)
    console.log(check_res)
}

test()
