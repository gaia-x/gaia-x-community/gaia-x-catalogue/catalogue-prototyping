'use strict'

const { Parser, Store } = require('n3')
const { HashMapDataset, Graph, PlanBuilder } = require('sparql-engine')

// Format a triple pattern according to N3 API:
// SPARQL variables must be replaced by `null` values
function formatTriplePattern (triple) {
    let subject = null
    let predicate = null
    let object = null
    if (typeof(triple.subject) != "string" || !triple.subject.startsWith('?')) {
        subject = triple.subject
    }
    if (typeof(triple.predicate) != "string" || !triple.predicate.startsWith('?')) {
        predicate = triple.predicate
    }
    if (typeof(triple.object) != "string" || !triple.object.startsWith('?')) {
        object = triple.object
    }
    return { subject, predicate, object }
}

// Format quads back into triples
function formatQuad(quad) {
    let triple = {};
    triple.subject = quad._subject
    triple.predicate = quad._predicate
    triple.object = quad._object
    return triple
}

// Wrap N3 in a way the sparql-engine package understands
class N3Graph extends Graph {
    constructor () {
        super()
        this._store = new Store()
        this._parser = new Parser()
    }

    parse (rdf) {
        return new Promise((resolve, reject) => {
            try {
                this._parser.parse(rdf).forEach(triple => {
                    this._store.addQuad(triple)
                })
                resolve()
            } catch (e) {
                reject(e)
            }
        })
    }

    insert (triple) {
        return new Promise((resolve, reject) => {
            try {
                this._store.addQuad(triple.subject, triple.predicate, triple.object)
                resolve()
            } catch (e) {
                reject(e)
            }
        })
    }
    
    delete (triple) {
        return new Promise((resolve, reject) => {
            try {
                this._store.removeQuad(triple.subject, triple.predicate, triple.object)
                resolve()
            } catch (e) {
                reject(e)
            }
        })
    }

    find (triple) {
        const { subject, predicate, object } = formatTriplePattern(triple)
        return Array.from(this._store.getQuads(subject, predicate, object), formatQuad)
    }

    estimateCardinality (triple) {
        const { subject, predicate, object } = formatTriplePattern(triple)
        return Promise.resolve(this._store.countQuads(subject, predicate, object))
    }
}


module.exports = {
    createGraph: () => {
        let graph = new N3Graph()
        let dataset = new HashMapDataset('http://example.org#default', graph)
        let query_builder = new PlanBuilder(dataset)
        return {graph, query_builder};
    }
}

function test() {
    let {graph, builder} = module.exports.createGraph()
    
    // Load some RDF data into the graph
    graph.parse(`
  @prefix foaf: <http://xmlns.com/foaf/0.1/> .
  @prefix : <http://example.org#> .
  :a foaf:name "a" .
  :b foaf:name "b" .
`)

    const query = `
  PREFIX foaf: <http://xmlns.com/foaf/0.1/>
  SELECT ?s ?name
  WHERE {
    ?s foaf:name ?name .
  }`

    // Get an iterator to evaluate the query
    const iterator = builder.build(query)

    // Read results
    iterator.subscribe(
        bindings => console.log('Find solutions:', bindings.toObject()),
        err => console.error('error', err),
        () => console.log('Query evaluation complete!')
    )

}

//test()
