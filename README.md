# GAIA-X Catalogue Prototyping

Prototyping environment to accompany the specification of the GAIA-X catalogue.

Start the service with `npm start`.

All Self-Descriptions from the folder `sd-cache` are loaded.

The service opens port 5000 for a minimal HTML interface and REST interfaces.

## Example Query (SPARQL)

```
SELECT ?org
WHERE {
    ?address <https://www.w3.org/2006/vcard/ns#postal-code> "80686" .
    ?org <http://w3id.org/gaia-x/core#hasLegallyBindingAddress> ?address .
}
```
